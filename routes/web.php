<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/sites/read', function () use ($router) {
    $response = [['siteid' => 15]];
    return response()->json($response);
});

$router->get('/rest/upsell/ma', function () use ($router) {
    $response = '[
      {
          "id": 17,
        "ma_id": 89,
        "name": "PervMom"
      },
      {
          "id": 16,
        "ma_id": 85,
        "name": "BlackValleyGirls"
      },
      {
          "id": 15,
        "ma_id": 86,
        "name": "Submissived"
      },
      {
          "id": 14,
        "ma_id": 80,
        "name": "Shoplyfter"
      },
      {
          "id": 13,
        "ma_id": 79,
        "name": "DadCrush"
      },
      {
          "id": 12,
        "ma_id": 75,
        "name": "DaughterSwap"
      },
      {
          "id": 11,
        "ma_id": 45,
        "name": "AnalEuro"
      },
      {
          "id": 10,
        "ma_id": 74,
        "name": "SisLovesMe"
      },
      {
          "id": 9,
        "ma_id": 73,
        "name": "PetiteTeens18"
      },
      {
          "id": 7,
        "ma_id": 72,
        "name": "Familystroke"
      },
      {
          "id": 6,
        "ma_id": 15,
        "name": "Teamskeet"
      },
      {
          "id": 5,
        "ma_id": 66,
        "name": "Blackstepdad"
      },
      {
          "id": 4,
        "ma_id": 64,
        "name": "Teensloveblackcocks"
      },
      {
          "id": 3,
        "ma_id": 63,
        "name": "Punishteens"
      },
      {
          "id": 2,
        "ma_id": 48,
        "name": "Exxxtrasmall"
      },
      {
          "id": 1,
        "ma_id": 49,
        "name": "Bffs"
      }
    ]';
    return response()->json(json_decode($response));
});

$router->get('/rest/upsell/site/{siteid}', function ($siteid) use ($router) {
    $file_name = storage_path('upsell/' . $siteid . '.json');
    $response = file_get_contents($file_name);
    return response()->json(json_decode($response));
});
